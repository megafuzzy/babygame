﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoMonster : MonoBehaviour {


    Animator vmAnimator = null;
    AudioClip[] vmClips;
    private AudioSource vmSounds;

    public bool menuIsActive = true;

    public AudioClip headBashAudio;
    public AudioClip fistBashAudio;
    public AudioClip cheerAudio;
    public AudioClip cryAudio;
    public AudioClip excitedAudio;
    public AudioClip tvStaticAudio;
    public AudioClip tvFixingAudio;
    public AudioClip focusAudio;
    public AudioClip squeekAudio;
    public AudioClip headBashFinalAudio;

    private bool animationPlaying = false;

    // Use this for initialization
    void Start () {
        
        vmAnimator = GetComponent<Animator>();
        vmSounds = GetComponent<AudioSource>();
        vmAnimator.SetTrigger("LookTV");
        vmClips = new AudioClip[10]; // { headBashAudio, fistBashAudio, cheerAudio, cryAudio, excitedAudio, tvStaticAudio, tvFixingAudio, focusAudio, squeekAudio, null };
        vmClips[0] = headBashAudio;
        vmClips[1] = fistBashAudio;
        vmClips[2] = cheerAudio;
        vmClips[3] = cryAudio;
        vmClips[4] = excitedAudio;
        vmClips[5] = tvStaticAudio;
        vmClips[6] = tvFixingAudio;
        vmClips[7] = focusAudio;
        vmClips[8] = squeekAudio;
        vmClips[9] = headBashFinalAudio;

        StartCoroutine(videoMonsterActionsTimer());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator videoMonsterActionsTimer()
    {
        //print(Time.time);
        vmAnimator.SetTrigger("LookTV");
        while (menuIsActive)
        {


            //for (int animationDice = 1; animationDice < 6; animationDice++)
            //{
            //    yield return new WaitForSeconds(15);
            //    if (animationDice == 1) vmAnimator.SetTrigger("BashHead");
            //    if (animationDice == 2) vmAnimator.SetTrigger("HitFist");
            //    if (animationDice == 3) vmAnimator.SetTrigger("Cry");
            //    if (animationDice == 4) vmAnimator.SetTrigger("GetExcited");
            //    if (animationDice == 5) vmAnimator.SetTrigger("FixTV");
            //}
            yield return new WaitForSeconds(10);

            if (!animationPlaying)
            {
                playRandomVMAnimation();
            }
            else
            {
                Debug.Log("Videomonster Random animation was prevented from playing");
            }
        }


    }
    //yield return new WaitForSeconds(3);
    ////print(Time.time);
    //if (LittleMonsterAnimator != null && MiddleMonsterAnimator != null && BigMonsterAnimator != null && !finalDancePlaying) //all animators should be ok at this point already
    //{
    //    radioSounds[0].Stop();
    //    radioSounds[1].Play();
    //    timeWhenFinalDanceStarted = Time.time;
    //    LittleMonsterAnimator.SetTrigger("Dance");
    //    MiddleMonsterAnimator.SetTrigger("Dance");
    //    BigMonsterAnimator.SetTrigger("Dance");
    //    finalDancePlaying = true;
    //}

    public void RandomAnimationsOnOff()
    {
        if (animationPlaying) animationPlaying = false;
        else animationPlaying = true;
        Debug.Log("Animation playing is " + animationPlaying + " so video monster random animations are " + !animationPlaying);
    }

    private void playRandomVMAnimation()
    {
        
        
        int animationDice = Random.Range(1, 6);    // 1 <= dice < 6
        if (animationDice == 1) vmAnimator.SetTrigger("BashHead");
        if (animationDice == 2) vmAnimator.SetTrigger("HitFist");
        if (animationDice == 3) vmAnimator.SetTrigger("Cry");
        if (animationDice == 4) vmAnimator.SetTrigger("GetExcited");
        if (animationDice == 5) vmAnimator.SetTrigger("FixTV");
        

    }

    public void playHeadBashSound()
    {
        vmSounds.PlayOneShot(vmClips[0]);
    }

    public void playHeadBashFinalSound()
    {
        vmSounds.PlayOneShot(vmClips[9]);
    }

    public void playFistBashSound()
    {
        vmSounds.PlayOneShot(vmClips[1]);
    }

    public void playCheerSound()
    {
        vmSounds.PlayOneShot(vmClips[2]);
    }

    public void playCrySound()
    {
        vmSounds.PlayOneShot(vmClips[3]);
    }

    public void playExcitedSound()
    {
        vmSounds.PlayOneShot(vmClips[4]);
    }

    public void playTVStaticSound()
    {
        //vmSounds.PlayOneShot(vmClips[9]);
    }

    public void playTVFixingSound()
    {
        vmSounds.PlayOneShot(vmClips[6]);
    }

    public void playVideoMonsterFocusSound()
    {

    }

    public void playVideoMonsterSqueekSound()
    {

    }

    public void playCheerAnimation()
    {
        //Debug.Log("PLaying cheer animation now");
        vmAnimator.SetTrigger("Cheer");
    }
}
