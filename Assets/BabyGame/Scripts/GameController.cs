﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class GameController : MonoBehaviour {

    public float blockInputDuration = 1f; //block touches for a given time
    public int babyCountUntilEnd = 20;
    private int babyCount = 0;
    public float startingInterval = 10f;
    private float currentInterval;
    public float reduceIntervalBy = 0.3f;
    public AudioClip exitSound;
    public AudioClip bgSound;
    //public int maxBabiesAtTime = 7;
    //public float secondsUntilBabyAutoDisappear = 20f;
    //private bool finalShowPlaying = false;

    private AudioSource gameSounds;
    public Baby scandicPrefab = null;
    public Baby asianPrefab = null;
    public Baby latinoPrefab = null;
    private List<Baby> babies = new List<Baby>();

    private bool blockTouches = false;
    private Touch[] myTouches;

    private InterstitialAd interstitial;

    // Use this for initialization
    void Start()
    {
        currentInterval = startingInterval;
        gameSounds = GetComponent<AudioSource>();

#if UNITY_ANDROID
        string appId = "ca-app-pub-8347667331834511~1833056739";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";
#else
            string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-8347667331834511/5389583397"; //my real ad unit for feed the monsters 
        //string adUnitId = "ca-app-pub-3940256099942544/1033173712"; //google sample ad unit for interstitial ads
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
        
        StartCoroutine(SpawnBabies());    
        
    }

    // Update is called once per frame
    void Update()
    {

        //check if game should be ended

        if (!blockTouches) updateTouch();

    }

    private void updateTouch()
    {
        myTouches = Input.touches;
        //Debug.Log("my touches length is : " + myTouches.Length);
        foreach (Touch t in myTouches) //loop through each finger
        {
            Debug.Log("touch detected: " + t.fingerId);
            //var inputPosition = currentTouchPosition(t.position);
            var phase = t.phase;
            //var fingerId = t.fingerId;

            switch (phase)
            {
                case TouchPhase.Began: //used only for pokes

                    Ray ray = Camera.main.ScreenPointToRay(t.position);
                    Debug.Log("Ray origin " + ray.origin + " and ray direction " + ray.direction);
                    RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 100f);


                    //GameObject crossHair = GameObject.FindGameObjectWithTag("Crosshair");
                    //crossHair.transform.position = hit.point;
                    Debug.Log("Ray hit something");
                    GameObject goHit = hit.transform.gameObject;

                    if (goHit.tag == "Background")
                    {
                        //Debug.Log("background hit");
                        //generateNewBaby(hit.point);
                        playBackGroundHitSound();

                    }
                    else if (goHit.tag == "Baby")
                    {
                        Debug.Log("improve baby mood detected");
                        improveBabyMood(goHit);
                    }
                    break;

                case TouchPhase.Moved: 

                    //This could be used to move babies around?

                    break;
                    


            }
        }
    }

    private void playBackGroundHitSound()
    {
        gameSounds.PlayOneShot(bgSound);
    }

    //public void blockTouchesForExit()
    //{
    //    blockTouches = true;
    //}

    //public void freeTouchesForExit()
    //{
    //    blockTouches = false;
    //}

    public void subtractBaby()
    {
        babyCount--;
    }

    private void improveBabyMood(GameObject baby)
    {
        Debug.Log("Trying to improve mood for baby: " + baby.ToString());
        //baby.GetComponent<Baby>().improveMood();
        baby.SendMessage("improveMood");
        //baby.improveMood();
    }
 

    private void generateNewBaby(Vector3 pos)
    {
        Debug.Log("Generate new baby at pos "+pos);

        Random rnd = new Random();
        int race = Random.Range(1, 4);
        Baby babyClone = null;

        if (race == 1) babyClone = Instantiate(scandicPrefab, new Vector3(pos.x, pos.y, 0), Quaternion.Euler(0, 0, 0));
        else if (race == 2) babyClone = Instantiate(asianPrefab, new Vector3(pos.x, pos.y, 0), Quaternion.Euler(0, 0, 0));
        else if (race == 3) babyClone = Instantiate(latinoPrefab, new Vector3(pos.x, pos.y, 0), Quaternion.Euler(0, 0, 0));
        else Debug.Log("Cannot find any baby race for int " + race);

        if (babyClone != null)
        {
            addToBabies(babyClone);
            babyCount++;
        }
        //babyClone.setPosition(pos);
        //babyClone.transform.position += pos;



        //instantiate
        //set random skin
        //set Animator

    }

    private void addToBabies(Baby newClone)
    {
        babies.Add(newClone);
        Debug.Log("Babies in list: " + babies.Count);
    }

    IEnumerator SpawnBabies()
    {
        while (babyCount <= babyCountUntilEnd)
        {
            yield return new WaitForSeconds(1); //always have at least 1 second interval
                                                //Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height/2, Camera.main.nearClipPlane+5)); //will get the middle of the screen
            Debug.Log("Current interval is " + currentInterval);
            
            Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(100, Screen.width-100), Random.Range(100, Screen.height-100), Camera.main.farClipPlane / 2));
            generateNewBaby(screenPosition);
            yield return new WaitForSeconds(currentInterval);
            currentInterval = currentInterval - reduceIntervalBy;
            if (babyCount == babyCountUntilEnd) break; 
        }
        
        StartCoroutine(explodeAllBabies());


    }

    private IEnumerator explodeAllBabies()
    {
        Debug.Log("Exploding babies from list, count is: " + babies.Count);
        foreach (Baby baby in babies)
        {
            Debug.Log("Trying to exit baby " + baby.ToString());
            yield return new WaitForSeconds(3.0f);
       
            baby.immediateExit();
        }
        exitGame();
    }

    public void exitGame()
    {
        MenuController.hasPlayedInThisSession = true;
        //RateBox.Instance.Show();
        gameSounds.PlayOneShot(exitSound);

        if (interstitial.IsLoaded())
        {
            Debug.Log("Show interstitial now as its loaded");
            interstitial.Show();
        }
        
        interstitial.Destroy();

        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
