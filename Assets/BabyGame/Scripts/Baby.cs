﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baby : MonoBehaviour {

    Animator animator;
    
    public AudioClip[] unhappyVoices = { null, null };
    public AudioClip[] contentVoices = { null, null, null };
    public AudioClip[] happyVoices = { null, null };
    public AudioClip exitVoice;
    public AudioClip popSound;

    GameObject gameController;

    AudioSource audioSource;

    public enum Race
    {
        Asian,
        Latino,
        Scandic
    };

    public enum Mood
    {
        Unhappy,
        Content,
        Happy,
        Exit
    }

    Race race;
    Mood mood;


    // Use this for initialization
    void Awake () {

        gameController = GameObject.FindGameObjectWithTag("GameController");
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        setRandomRace();
        setRandomMood();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setRandomRace()
    {
        //int raceInt = Random.Range(0, 2);

        //if (raceInt == 0) race = Race.Asian;
        //if (raceInt == 0) race = Race.Latino;
        //if (raceInt == 0) race = Race.Scandic;

        race = Race.Scandic; //in beta version

    }

    public void setPosition(Vector2 pos)
    {
        Debug.Log("New position given from gamecontroller: " + pos);
        transform.position = pos;
    }

    public void improveMood()
    {
        Debug.Log("Baby current mood before improvement is " + mood);
        audioSource.PlayOneShot(popSound);
        mood++;
        playMoodAnimation(mood);
        Debug.Log("Baby current mood after improvement is " + mood);
    }

    public void setRandomMood()
    {
        //int moodInt = Random.Range(0, 2);
        Debug.Log("Set random mood");
        mood = (Mood)Random.Range(0, 3);
        playMoodAnimation(mood);

        //if (raceInt == 0) race = Race.Asian;
        //if (raceInt == 0) race = Race.Latino;
        //if (raceInt == 0) race = Race.Scandic;
    }

    public void immediateExit()
    {
        Debug.Log("immediateExit called");

        StartCoroutine(exitBaby());
    }

    public void playMoodAnimation(Mood mood)
    {

        Debug.Log("Play mood animation");
        switch (mood)
        {
            case Mood.Unhappy:
                Debug.Log("Mood unhappy: " + mood.ToString());
                animator.SetTrigger("Unhappy");
                break;
            case Mood.Content:
                Debug.Log("Mood content: " + mood.ToString());
                animator.SetTrigger("Content");
                break;
            case Mood.Happy:
                Debug.Log("Mood happy: " + mood.ToString());
                animator.SetTrigger("Happy");
                break;
            case Mood.Exit:
                StartCoroutine(exitBaby());
                
                
                break;
        }

        
    }

    private IEnumerator exitBaby()
    {
        Debug.Log("Exiting baby");
        gameObject.GetComponent<SpriteRenderer>().sortingOrder++;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        gameObject.GetComponent<CircleCollider2D>().enabled = false;

        int x = 0;  //Screen.width / 2;
        int y = 0;  //Screen.height / 2;
        Debug.Log("x is " + x + " and y " + y);
        Vector3 center = new Vector3(x, y);
        
        var currentPos = transform.position;
        var t = 0f;
        var timeToMove = 2.5f;
        animator.SetTrigger("Exit");
        //gameController.SendMessage("blockTouchesForExit");
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            transform.position = Vector3.Lerp(currentPos, center, t);
            yield return null;
        }

        
        //yield return new WaitForSeconds(1.5f);
        gameObject.SetActive(false);

        gameController.SendMessage("subtractBaby");
        //gameController.SendMessage("freeTouchesForExit");
    }

    public void playRandomCrySound()
    {
        Debug.Log("Random cry");
        int cryInt = Random.Range(0, 2);
        Debug.Log("int was " + cryInt);
        audioSource.PlayOneShot(unhappyVoices[cryInt]);
    }

    public void PlayRandomContentSound()
    {
        Debug.Log("Random content");
        int contInt = Random.Range(0, 3);
        Debug.Log("int was " + contInt);
        audioSource.PlayOneShot(contentVoices[contInt]);
    }

    public void PlayRandomHappySound()
    {
        Debug.Log("Random happy");
        int hapInt = Random.Range(0, 2);
        Debug.Log("int was " + hapInt);
        audioSource.PlayOneShot(happyVoices[hapInt]);
    }

    public void PlayExitSound()
    {
        Debug.Log("Exit sound");
        audioSource.PlayOneShot(exitVoice);
    }
}
