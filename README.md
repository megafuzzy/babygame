# Project description

Megafuzzy Baby Game is an amazingly fun and simple minigame for babies, toddlers and even preschoolers. Those cute babies just keep popping up, some laughing, some babbling, some crying their eyes out. 

Your toddler will have a wonderful time trying to keep the mass of babies in check by tapping them and improving their moods.

* Authentic baby sounds
* 3 different kinds of babies with unique animations
* Tons of FUN

**This application develops:**

* Motor skills
* Reactions
* Social interaction
* Empathy
* Identifying with feelings and expressions

**Please note, this project has not been migrated to 64-bit and therefore is deprecated and not available in Play store anymore. This repository is here only for the job hunting purposes**

# My role in development and technologies used

* The game was developed solely by myself, using Unity3D and scripting with C#. 
* All credits for the graphics go to https://carolinamorera.com/
* Baby sounds recorded by my own 2 year old
* Please check Screenshots folder for pics!
* Demo reel video for all 3 MegaFuzzy games can be found from "MonstersMunchtime" project, in Screenshots folder.